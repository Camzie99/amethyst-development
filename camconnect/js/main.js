var username = getCookie('username');
var chan = getCookie('channel');
var style = getCookie('style');
var d = new Date();
var lastmessage = d.getTime();
var started = false;
var curdata = "";
var datas;
var datats;
var dataths;
var running = false;
var lock = false;

function c()
{
	var n=$('.c').attr('id');
	var c=n;
	$('.c').text(c);
	setInterval(function()
	{
		c--;
		if(c >= 0)
		{
			$('.c').text(c);
		}
		if(c < 0)
		{
			$('.c').text(0);
		}
	}, 1200);
}

function rotate()
{
	$("#amethystlogo").removeClass("amethystlogo");
	$("#amethystlogo").addClass("amethystlogorotated");
}

$(document).ready(function()
{
	c();
    $("#curhover").fadeOut("fast");
    $("#loading").delay(7200).fadeOut("slow");
    $("#loading").delay(7200).fadeOut("slow");
    $("#circle").delay(6000).fadeOut("fast");
    $(".c").delay(6000).fadeOut("fast");
    $("#loaded").delay(6200).fadeIn("fast");
    setTimeout(rotate, 6600);
	if(style == null || style == "" || style == "null" || style.length < 1)
	{
		document.cookie=document.cookie + "style=blue";
		style="blue";
	}
	var head  = document.getElementsByTagName('head')[0];
	var link  = document.createElement('link');
	link.rel  = 'stylesheet';
	link.type = 'text/css';
	link.href = 'css/' + style + '.css';
	link.media = 'all';
    head.appendChild(link);
	setTimeout(start, 8400);
	$('#emoticons').hide(0, 'none');
	$("#emoticons").draggable();
	$("#emoticons").resizable();
	refresh();
	scroll();
});

function start()
{
	while(username == null || username == "" || username == "null" || username.length >= 16)
	{
		$('html, body').animate({ scrollTop: 0 }, 'fast');
		username = prompt('Please enter your desired username.');
		document.cookie="username=" + username;
	}
	while(chan == null || chan == "" || chan == "null" || chan.length < 1)
	{
		$('html, body').animate({ scrollTop: 0 }, 'fast');
		chan = prompt('Please enter a channel name.');
		document.cookie="channel=" + chan;
	}
	refresh();
	scroll();
	started = true;
}

window.setInterval(function()
{
	if(started)
	{
		while(username == null || username == "" || username == "null" || username.length >= 16)
		{
			$('html, body').animate({ scrollTop: 0 }, 'fast');
			username = prompt('Please enter your desired username.');
			document.cookie="username=" + username;
		}
		while(chan == null || chan == "" || chan == "null" || chan.length < 1)
		{
			$('html, body').animate({ scrollTop: 0 }, 'fast');
			chan = prompt('Please enter a channel name.');
			document.cookie="channel=" + chan;
		}
		var query = 'chat.php?channel=' + chan + '&name=' + username + ' #log > *';
		if(document.getElementById('autoRefresh').checked)
		{
			refresh();
		}
		if(document.getElementById('autoScroll').checked)
		{
			scroll();
		}
	}
}, 500);

function refresh()
{
	if(!running)
	{
		running = true;
		$.ajax(
		{
			url: "chat.php",
			type:'GET',
			data:
			{
				name: username,
				channel: chan
			},
			success: function(data)
			{ 
				datas = data;
			},            
		}).then(function()
		{
			$.ajax(
			{
				url: "check.php",
				type: "POST",
				data:
				{
					message: datas
				},
				success: function(datat)
				{
					datats = datat;
				},
			}).then(function()
			{
				$.ajax(
				{
					url: "check.php",
					type: "POST",
					data:
					{
						message: curdata
					},
					success: function(datath)
					{
						dataths = datath;
					},
				}).then(function()
				{
					if(datats.length == ("Welcome to CamConnect, you are currently connected to the channel named: " + chan + ".").length && curdata != datats)
					{
						document.getElementsByTagName('pre')[0].innerHTML = datats;
						curdata = datats;
					}
					else if(datats.length != dataths.length)
					{
						jQuery('pre').append(datats.substring(dataths.length));
						curdata = curdata + datats.substring(dataths.length);
					}
					running = false;
				});
			});
		});
		jQuery('#channels').load('channellist.php');
	};
}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++)
    {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function deleteCookie()
{
	document.cookie="username=" + username;
	chan = null;
	while(chan == null || chan == "" || chan == "null" || chan.length < 1)
	{
		$('html, body').animate({ scrollTop: 0 }, 'fast');
		chan = prompt('Please enter a channel name.');
		document.cookie="channel=" + chan;
	}
	curdata = "";
	$('pre').empty();
	refresh();
}

function scroll()
{
	document.getElementById('log').scrollTop=document.getElementById('log').scrollHeight-1;
}

function sendCommand()
{
	var newDate = new Date();
	if(newDate.getTime() - lastmessage > 2500)
	{
		lastmessage = newDate.getTime();
		document.getElementById('command').select();
		var data = document.getElementById('command').value;
		$.ajax(
		{
			url: "send.php",
			type:'POST',
			data:
			{
				name: username,
				channel: chan,
				command: data
			},
			success: function(msg)
			{
				document.getElementById('command').value = '';
			}               
		});
	}
	if(document.getElementById('command').value.indexOf('<') > -1)
	{
		alert('Custom HTML tags cannot be sent into the chat.');
	}
	return false;
}

function changeStyle()
{
    var e = document.getElementsByTagName("select")[0];
    var f = e.options[e.selectedIndex].value;
    document.cookie='style=' + f; window.location.reload();   
}

function add(string)
{
	document.getElementById('command').value = document.getElementById('command').value + string;
	document.getElementById('command').select();
}

$(document).on('mousemove', 'img', function(e)
{
	clazz = $(e.target).attr('class');
	if(clazz != "nojs" && clazz != "emoticon" && clazz != "emotebutton" && clazz != "arrow" && clazz != "exit" && !lock)
	{
		$('#curhover').fadeIn("slow");
		var image = document.createElement("img");
		image.src=$(e.target).attr('src');
		image.className="nojs";
		$('#curhover').html(image);
		lock = true;
	}
});

$(document).on('mouseleave', 'img', function(e)
{
	$.when($('#curhover').fadeOut("fast")).done(function()
	{
		lock = false;
	});
});

$(document).on('touchstart', 'img', function(e)
{
	clazz = $(e.target).attr('class');
	if(clazz != "nojs" && clazz != "emoticon" && clazz != "emotebutton" && clazz != "arrow" && clazz != "exit" && !lock)
	{
		$('#curhover').fadeIn("slow");
		var image = document.createElement("img");
		image.src=$(e.target).attr('src');
		image.className="nojs";
		$('#curhover').html(image);
		lock = true;
	}
});

$(document).on('touchend', function(e)
{
	$('#curhover').fadeOut("fast");
	lock = false;
});


