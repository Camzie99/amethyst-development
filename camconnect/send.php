<?php
	function replaceAll($input)
	{
		$replace = array(
		":smile:" => "<img class='emoticon' src='emoji/smile.png' />",
		":sad:" => "<img class='emoticon' src='emoji/sad.png' />",
		":bigsmile:" => "<img class='emoticon' src='emoji/big-smile.png' />",
		":rocket:" => "<img class='emoticon' src='emoji/rocket.png' />",
		":star:" => "<img class='emoticon' src='emoji/star.png' />",
		":car:" => "<img class='emoticon' src='emoji/car.png' />",
		":building:" => "<img class='emoticon' src='emoji/bank-2.png' />",
		":bomb:" => "<img class='emoticon' src='emoji/bomb.png' />",
		":flash:" => "<img class='emoticon' src='emoji/flash.png' />",
		":tower:" => "<img class='emoticon' src='emoji/town-tower-1.png' />",
		":windows:" => "<img class='emoticon' src='emoji/window.png' />",
		":plane:" => "<img class='emoticon' src='emoji/plane_1.png' />",
		":apple:" => "<img class='emoticon' src='emoji/apple.png'>",
		":iexplorer:" => "<img class='emoticon' src='emoji/internet_explorer.png' />",
		":gchrome:" => "<img class='emoticon' src='emoji/google_chrome.png' />",
		":ffox:" => "<img class='emoticon' src='emoji/firefox.png' />",
		":tongue:" => "<img class='emoticon' src='emoji/tongue.png' />",
		":cry:" => "<img class='emoticon' src='emoji/cry.png' />",
		":surprise:" => "<img class='emoticon' src='emoji/surprised.png' />",
		 ":cute:" => "<img class='emoticon' src='emoji/cute.png' />",
		 ":wink:" => "<img class='emoticon' src='emoji/wink.png' />",
		 ":monitor:" => "<img class='emoticon' src='emoji/monitor_1.png' />",
		 ":mail:" => "<img class='emoticon' src='emoji/mail_envelope.png' />",
		 ":phone:" => "<img class='emoticon' src='emoji/cell_phone.png' />",
		 ":sunny:" => "<img class='emoticon' src='emoji/sunny.png' />",
		 ":rain:" => "<img class='emoticon' src='emoji/light_rain.png' />",
		 ":snow:" => "<img class='emoticon' src='emoji/snow_2.png' />",
		 ":bin:" => "<img class='emoticon' src='emoji/trash.png' />",
		 ":globe:" => "<img class='emoticon' src='emoji/global1.png' />",
		 ":edge:" => "<img class='emoticon' src='emoji/edge.png' />");
		 
		 return strtr($input, $replace);
	}
	
	function replace($input)
	{
		$find = array
		(
			'~\[b\](.*?)\[/b\]~s',
			'~\[i\](.*?)\[/i\]~s',
			'~\[u\](.*?)\[/u\]~s',
			'~\[quote\](.*?)\[/quote\]~s',
			'~\[size=(.*?)\](.*?)\[/size\]~s',
			'~\[color=(.*?)\](.*?)\[/color\]~s',
			'~\[url\]((?:ftp|https?)://.*?)\[/url\]~s',
			'~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s'
		);

		$replace = array
		(
			'<b>$1</b>',
			'<i>$1</i>',
			'<span style="text-decoration:underline;">$1</span>',
			'<pre>$1</'.'pre>',
			'<span style="font-size:$1px;">$2</span>',
			'<span style="color:$1;">$2</span>',
			'<a href="$1">$1</a>',
			'<img src="$1" alt="" />'
		);

		echo(replaceimgs($input));
		return preg_replace($find, $replace, htmlspecialchars(replaceimgs($input)));
	}
	
	function replaceimgs($input)
	{
		$matches = array();
		preg_match('~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s', $input, $matches);
		$i = 0;
		foreach($matches as $match)
		{
			if($i > 0)
			{
				$id = gen_uuid();
				$exts = array();
				preg_match('~(?:https?://.*?\.(jpg|jpeg|gif|png|bmp))~s', $match, $exts);
				print_r($exts);
				$extension = $exts[1];
				downloadFile($match, 'downloaded/' . $id . '.' . $extension);
				$input = str_replace($match, 'https://www.amethystdevelopment.co.uk/camconnect/downloaded/' . $id . '.' . $extension, $input);
			}
			$i++;
		}
		return $input;
	}
	
	function gen_uuid()
	{
    	return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,

			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    	);
	}	
	function downloadFile ($url, $path)
	{
		$newfname = $path;
		$file = fopen ($url, "rb");
		if ($file)
		{
			$newf = fopen ($newfname, "wb");
			if ($newf)
			while(!feof($file))
			{
				fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
			}
			
	  		if ($newf)
	  		{
				fclose($newf);
			}
		}

		if ($file)
		{
			fclose($file);
	  	}

 	}

	if(isset($_POST['command']) and isset($_POST['name']) and isset($_POST['channel']))
	{	
		str_replace("<", "&lt", $_POST['command']);
		$file = fopen("banned.txt","r");
		while (($buffer = fgets($file, 2048)) !== false)
		{
        	if(strpos($_POST['name'], $buffer) !== false)
        	{
        		die('<div id="log">You are banned from accessing this resource.</div>');
        	}
    	}
		fclose($file);
		
		if(strpos($_POST['command'], '/') !== false and $_POST['name'] === "##SENDCOMMAND##")
		{
			file_put_contents("banned.txt", "\n" . str_replace('/', '', $_POST['command']), FILE_APPEND);
		}
		else
		{
			date_default_timezone_set("UTC");
       		if(strpos($_POST['command'], '<') !== false)
       		{
       			die('Cannot send custom HTML tags into the chat.');
       		}
       		else
       		{
       			$tosave = $_POST['command'];
				if(preg_match("/:progress:\d*:\d*:/", $_POST['command']))
				{
					$matches = array();
					preg_match("/:progress:\d*:\d*:/", $_POST['command'], $matches);
					foreach($matches as $bar)
					{
						$exploded = explode(":", $bar);
						$max = $exploded[3];
						$value = $exploded[2];
						$tosave = preg_replace("/:progress:\d*:\d*:/", "<progress max='" . $max . "' value='" . $value . "' />", $tosave, 1);
					}
				}
				if(preg_match("/:meter:\d*:\d*:/", $_POST['command']))
				{
					$matches = array();
					preg_match("/:meter:\d*:\d*:/", $_POST['command'], $matches);
					foreach($matches as $bar)
					{
						$exploded = explode(":", $bar);
						$max = $exploded[3];
						$value = $exploded[2];
						$tosave = preg_replace("/:meter:\d*:\d*:/", "<meter max='" . $max . "' value='" . $value . "' />", $tosave, 1);
					}
				}
    			file_put_contents('channels/' . str_replace(' ', '_', $_POST['channel']) . '.txt', "\n[" . date("H:i:s") . "] [" . $_POST['name'] . "]: " . replace(replaceAll($tosave)), FILE_APPEND);
			}
		}
	}
	else
	{
		if(isset($_POST['name']))
		{
			$file = fopen("banned.txt","r");
			while (($buffer = fgets($file, 4096)) !== false)
			{
        		if(strpos($_POST['name'], $buffer) !== false)
        		{
        			die('<div id="log">You are banned from accessing this resource.</div>');
        		}
    		}
			fclose($file);
		}
	}
?>