<head>
<title>CamConnect</title>

<!-- Latest version of JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Bootstrap optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Main custom JS -->
<script src="js/main.js"></script>

<!-- Main custom CSS -->
<link rel="stylesheet" href="css/main.css">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Oleo+Script+Swash+Caps:700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>

<!-- jQuery UI -->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="js/touchpunch.js"></script>
<link rel="stylesheet" href="css/jQueryUI.css" type="text/css">

<!-- Amethyst Logo CSS -->
<link rel="stylesheet" href="https://amethystdevelopment.co.uk/css/amethystlogo.css">

<script>
</script>
</head>
<body>
<div class="loading" id="loading"><center><h1 class="loadingtext" id="loadingtext">Welcome to CamConnect ~ Loading Awesomeness!</h1><h4>Content will be available in</h4><div class='c' id='5'></div><div id="loaded" style="display: none"><!-- Amethyst Logo -->
                    		<img src="https://amethystdevelopment.co.uk/img/amdev.svg" id="amethystlogo" class="amethystlogo" /></div></center><svg id='circle' style='margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;' width='25%' xmlns="https://www.w3.org/2000/svg" timelineBegin="true" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><defs><filter id="uil-ring-shadow" x="-100%" y="-100%" width="300%" height="300%"><feOffset result="offOut" in="SourceGraphic" dx="0" dy="0"></feOffset><feGaussianBlur result="blurOut" in="offOut" stdDeviation="0"></feGaussianBlur><feBlend in="SourceGraphic" in2="blurOut" mode="normal"></feBlend></filter></defs><path d="M10,50c0,0,0,0.5,0.1,1.4c0,0.5,0.1,1,0.2,1.7c0,0.3,0.1,0.7,0.1,1.1c0.1,0.4,0.1,0.8,0.2,1.2c0.2,0.8,0.3,1.8,0.5,2.8 c0.3,1,0.6,2.1,0.9,3.2c0.3,1.1,0.9,2.3,1.4,3.5c0.5,1.2,1.2,2.4,1.8,3.7c0.3,0.6,0.8,1.2,1.2,1.9c0.4,0.6,0.8,1.3,1.3,1.9 c1,1.2,1.9,2.6,3.1,3.7c2.2,2.5,5,4.7,7.9,6.7c3,2,6.5,3.4,10.1,4.6c3.6,1.1,7.5,1.5,11.2,1.6c4-0.1,7.7-0.6,11.3-1.6 c3.6-1.2,7-2.6,10-4.6c3-2,5.8-4.2,7.9-6.7c1.2-1.2,2.1-2.5,3.1-3.7c0.5-0.6,0.9-1.3,1.3-1.9c0.4-0.6,0.8-1.3,1.2-1.9 c0.6-1.3,1.3-2.5,1.8-3.7c0.5-1.2,1-2.4,1.4-3.5c0.3-1.1,0.6-2.2,0.9-3.2c0.2-1,0.4-1.9,0.5-2.8c0.1-0.4,0.1-0.8,0.2-1.2 c0-0.4,0.1-0.7,0.1-1.1c0.1-0.7,0.1-1.2,0.2-1.7C90,50.5,90,50,90,50s0,0.5,0,1.4c0,0.5,0,1,0,1.7c0,0.3,0,0.7,0,1.1 c0,0.4-0.1,0.8-0.1,1.2c-0.1,0.9-0.2,1.8-0.4,2.8c-0.2,1-0.5,2.1-0.7,3.3c-0.3,1.2-0.8,2.4-1.2,3.7c-0.2,0.7-0.5,1.3-0.8,1.9 c-0.3,0.7-0.6,1.3-0.9,2c-0.3,0.7-0.7,1.3-1.1,2c-0.4,0.7-0.7,1.4-1.2,2c-1,1.3-1.9,2.7-3.1,4c-2.2,2.7-5,5-8.1,7.1 c-0.8,0.5-1.6,1-2.4,1.5c-0.8,0.5-1.7,0.9-2.6,1.3L66,87.7l-1.4,0.5c-0.9,0.3-1.8,0.7-2.8,1c-3.8,1.1-7.9,1.7-11.8,1.8L47,90.8 c-1,0-2-0.2-3-0.3l-1.5-0.2l-0.7-0.1L41.1,90c-1-0.3-1.9-0.5-2.9-0.7c-0.9-0.3-1.9-0.7-2.8-1L34,87.7l-1.3-0.6 c-0.9-0.4-1.8-0.8-2.6-1.3c-0.8-0.5-1.6-1-2.4-1.5c-3.1-2.1-5.9-4.5-8.1-7.1c-1.2-1.2-2.1-2.7-3.1-4c-0.5-0.6-0.8-1.4-1.2-2 c-0.4-0.7-0.8-1.3-1.1-2c-0.3-0.7-0.6-1.3-0.9-2c-0.3-0.7-0.6-1.3-0.8-1.9c-0.4-1.3-0.9-2.5-1.2-3.7c-0.3-1.2-0.5-2.3-0.7-3.3 c-0.2-1-0.3-2-0.4-2.8c-0.1-0.4-0.1-0.8-0.1-1.2c0-0.4,0-0.7,0-1.1c0-0.7,0-1.2,0-1.7C10,50.5,10,50,10,50z" fill="#8e0fe4" filter="url(#uil-ring-shadow)"><animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" dur="2.3s"></animateTransform></path></svg>
</div>
<div id="content">
  <center>
  <div class="maintitlediv"><h1 class="maintitle">CamConnect</h1></div>
  <button onClick="refresh();"/>
  Manual Refresh
  </button>
  <br />
  <button onClick="deleteCookie();"/>
  Change Current Channel
  </button>
  <div id="channels">
    <?php
    	include('channellist.php');
	?>
  </div>
  <hr id="horrule"/>
  <div id="log" style="height:65%; overflow-y:scroll; padding: 1px;">
  <pre></pre>
</div>
<label for="autoScroll">Auto-Scroll</label>
<input id="autoScroll" type="checkbox" checked/>
<label for="autoRefresh">Auto-Refresh</label>
<input id="autoRefresh" type="checkbox" checked/>
<form>
  <label for="command">Message</label>
  <input type='text' id="command" cols="40" rows="5" maxlength="2000" onkeypress="if(event.keyCode == 13){ return sendCommand(); }">
  </input>
</form>
  <button id="send" onClick='sendCommand()'>Send Message</button>
  <br />
  <button onClick='return false;' style="border-radius:50px;" class="emotebutton"><img style="height: 50px; width:50px;" id='hideshow' onClick="
    var div = document.getElementById('emoticons');
    if (div.style.display !== 'none')
    {
        $('#emoticons').hide(400, 'swing');
    }
    else
    {
        $('#emoticons').show(400, 'swing');
    }" src="emoji/smile.png" class='emoticon'/>
  </button>
  <br />
  <br />
<br /><br />
<div id="emoticons" class="emoticons">
  <h3>Emoticons (Click to Add)</h3>
  <img class="emotebutton" onClick="add(':smile: ')" src='emoji/smile.png' />
  <img class="emotebutton" onClick="add(':bigsmile: ')" src='emoji/big-smile.png' />
  <img class="emotebutton" onClick="add(':sad: ')" src='emoji/sad.png' />
  <img class="emotebutton" onClick="add(':cry: ')" src='emoji/cry.png' />
  <img class="emotebutton" onClick="add(':cute: ')" src='emoji/cute.png' />
  <img class="emotebutton" onClick="add(':wink: ')" src='emoji/wink.png' />
  <img class="emotebutton" onClick="add(':surprise: ')" src='emoji/surprised.png' />
  <img class="emotebutton" onClick="add(':tongue: ')" src='emoji/tongue.png' />
  <img class="emotebutton" onClick="add(':phone: ')" src='emoji/cell_phone.png' />
  <img class="emotebutton" onClick="add(':mail: ')" src='emoji/mail_envelope.png' />
  <img class="emotebutton" onClick="add(':monitor: ')" src='emoji/monitor_1.png' />
  <img class="emotebutton" onClick="add(':sunny: ')" src='emoji/sunny.png' />
  <img class="emotebutton" onClick="add(':rain: ')" src='emoji/light_rain.png' />
  <img class="emotebutton" onClick="add(':snow: ')" src='emoji/snow_2.png' />
  <img class="emotebutton" onClick="add(':bin: ')" src='emoji/trash.png' />
  <img class="emotebutton" onClick="add(':globe: ')" src='emoji/global1.png' />
  <img class="emotebutton" onClick="add(':gchrome: ')" src='emoji/google_chrome.png' />
  <img class="emotebutton" onClick="add(':iexplorer: ')" src='emoji/internet_explorer.png' />
  <img class="emotebutton" onClick="add(':edge: ')" src='emoji/edge.png' />
  <img class="emotebutton" onClick="add(':ffox: ')" src='emoji/firefox.png' />
  <img class="emotebutton" onClick="add(':rocket: ')" src='emoji/rocket.png' />
  <img class="emotebutton" onClick="add(':star: ')" src='emoji/star.png' />
  <img class="emotebutton" onClick="add(':bomb: ')" src='emoji/bomb.png' />
  <img class="emotebutton" onClick="add(':car: ')" src='emoji/car.png' />
  <img class="emotebutton" onClick="add(':building: ')" src='emoji/bank-2.png' />
  <img class="emotebutton" onClick="add(':tower: ')" src='emoji/town-tower-1.png' />
  <img class="emotebutton" onClick="add(':flash: ')" src='emoji/flash.png' />
  <img class="emotebutton" onClick="add(':plane: ')" src='emoji/plane_1.png' /> 
  <img class="emotebutton" onClick="add(':windows: ')" src='emoji/window.png' />
  <img class="emotebutton" onClick="add(':apple: ')" src='emoji/apple.png' />
  <div class="arrow"></div>
  <div class="exit" onClick="
    var div = document.getElementById('emoticons');
    if (div.style.display !== 'none')
    {
        $('#emoticons').hide(400, 'swing');
    }
    else
    {
        $('#emoticons').show(400, 'swing');
    }"></div>
</div>
<div id="curhover" class="curhover">
</div>
</center>
  <footer>
  <div class="mainfooter">
  	All credits for emoticons and backgrounds can be found at the <a href='credits.php'>credits page</a>. Massive thanks to all all those who have made this possible!<br />
    Please note, this software is not complete, it has many issues and bugs that should hopefully be worked out over the coming weeks! <br />
  </div>
  <div class="styleswitcher">
  	Don't like the look of the page? You can switch the background colours using this tool.
  	<br />
  	<select onchange="changeStyle()">
  	<option>Please Select A Colour</option>
	<option value="black">Black</option>
	<option value="blue">Blue</option>
	<option value="darkblue">Dark Blue</option>
	<option value="brown">Brown</option>
	<option value="green">Green</option>
	<option value="darkgreen">Dark Green</option>
	<option value="magenta">Magenta</option>
	<option value="orange">Orange</option>
	<option value="pink">Pink</option>
	<option value="purple">Purple</option>
	<option value="darkpurple">Dark Purple</option>
	<option value="red">Red</option>
	<option value="seafoam">Seafoam</option>
	<option value="yellow">Yellow</option>
	<option value="white">White</option>
	</select>
	<br />
 	Copyright &copy; Cameron Redmore 2015 | All Rights Reserved
  </div>
  </footer>
</div>
</body>