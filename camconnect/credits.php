<!DOCTYPE html>
<html>
<head>
<title>CamMessenger | Credits Page</title>
<link rel="stylesheet" href="css/main.css" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Oleo+Script+Swash+Caps:700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
</head>
<body>
<h1>I can't thank these awesome people enough for providing the resources I used in this website!</h1>
<ul>
	<li>All emoticons: <a href='http://simpleicon.com'>http://simpleicon.com</a>.</li>
	<li>Seafoam background: <a href='http://blubkool.deviantart.com/art/Sky-Background-261770566'>blubkool</a>.</li>
	<li>Green background: obtained <a href='http://wallpaperspal.com/wp-content/uploads/Green-Polygons-Pattern-Wallpaper.jpg'>here</a>.</li>
	<li>Dark Blue background: obtained <a href='http://wallpaperswa.com/thumbnails/detail/20120704/blue%20patterns%20textures%201920x1200%20wallpaper_wallpaperswa.com_15.jpg'>here</a>.</li>
	<li>Blue background: this was on multiple websites but the site I obtained it from credits it to <a href='http://customize.org/wallpapers/60434'>darinka</a>.</li>
	<li>Red background: this was on multiple websites and I could not find any specific credits. The website I obtained the image from can be found <a href='http://beautyartpics.com/hdpics/tag/red-color'>here</a>.</li>
	<li>Orange background: this image has no credits attached but it was obtained from <a href='http://picsfair.com/orange-background.html'>here</a>.</li>
	<li>Brown background: this image has no credits attached but it was obtained from <a href='http://editwallpaper.com/iphoneandroid/art-abstract-brown-wallpaper-wide-hd.html'>here</a>.</li>
	<li>Yellow background: this was on multiple websites and I could not find any specific creditation. The website I obtained the image from can be found <a href='http://wallpaper222.com/explore/yellow-patterns/'>here</a>.</li>
	<li>Purple background: this was on multiple websites and I could not find any specific creditation. The website I obtained the image from can be found <a href='http://wallpapercolor.net/purple-wallpapers/light-purple-background-designs-wallpaper'>here</a>.</li>
	<li>Dark Purple background: obtained <a href='http://imgbuddy.com/cool-dark-purple-background.asp'>here</a>.</li>
	<li>Black background: obtained <a href='http://www.resimsi.com/preview/large/9137/blue-and-black-background.html'>here</a>.</li>
	<li>White background: obtained <a href='http://www.psdgraphics.com/file/white-background.jpg'>here</a>.</li>
	<li>Dark Green background: obtained <a href='http://photographyofgrace.com/3dTextures/Textures/bluegreen/AbstractBackground_22.jpg'>here</a>.</li>
	<li>Pink background: obtained <a href='http://all-free-download.com/free-vector/download/pink_background_pattern_vector_275583.html'>here</a>.</li>
	<li>Magenta background: obtained <a href='http://cherrycreekcreative.com/?attachment_id=585'>here</a>.</li>
	</ul>
<a href='index.php'>Return to CamMessenger</a>
</body>
</html>